# videoutil documentation

Package to play videos with annotations in .npy format or to create new videos with the annotations included.

## How to use it

1. Open a terminal window, activate your python environment and go to a suitable location.

2. Download the package:

    `git clone https://delaRochaLab@bitbucket.org/delaRochaLab/videoutil.git`

3. Go to the package directory.

    `cd videoutil`

4. Install it (remember to have your desired environment activated):

    `pip install -e .`
    
5. To play the video:

    `videotutil 'path_to_video_file'`
    
5. To record a new video with the annotations:

    `videotutil -r 'path_to_video_file'`
    
The annotations file must be located in the same folder than the video and they must have the same name
(video with the extension .avi and annotations with the extension .npz).


## Controls

When playing video:
- z to go back 100 seconds
- x to go back 10 seconds
- c to go forward 10 seconds
- v to go forward 100 seconds
- space to pause / play
- esc to exit